# はじめに
Pythonでファミコンのゲームソフト「スーパーマリオ」をAIにクリアさせるプログラム。

以下、2種類を実装。

- DQN(Deep Q Network)を用いたAI
- GA(遺伝的アルゴリズム)を用いたAI

# 実行環境

- Linux
- Anaconda
- FCUEX


# 実行手順
### 1. 実行環境を整える

### 2. リポジトリのクローン
```bash
git clone https://gitlab.com/teten927/mario-ai.git
```

### 3. OpenAI用リポジトリをクローン
```bash
cd mario-ai
git clone https://github.com/ppaquette/gym-super-mario.git
```

### 4. conda環境の作成
```bash
conda env create -f envs/conda_env.yaml
conda activate mario
```

## [DQN]
### 5. 前準備
```
cd dqn
cp ../gym-super-mario/ppaquette_gym_super_mario .
```

### 6. パラメータの変更
main.pyの中の各パラメータを学習したいパラメータに変更する。

### 7. 実行
```
python main.py model
               ^^^^^
                保存するモデル名またはロードしたいモデル名(省略可)
```

## [GA]
### 5. 前準備
```
cd ga
cp ../gym-super-mario/ppaquette_gym_super_mario .
```

### 6. パラメータの変更
main.pyの中の各パラメータを学習したいパラメータに変更する。

### 7. 実行
```
python main.py
```
