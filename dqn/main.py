#  coding: cp932
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.initializers import TruncatedNormal
import tensorflow.keras.backend as kb
import gym
import ppaquette_gym_super_mario
import random
import numpy as np
import os
import cv2
import sys

screen_size = (13, 16, 1)
resized_screen_size = (13, 16, 1)

class Agent:

    def __init__(
        self,
        mario_course = 'ppaquette/SuperMarioBros-1-1-Tiles-v0',
        loop_count = 100,
        init_randam_num = 10,
        greedy_rate = 0.1,
        extreme_greedy_rate = 0.1,
        extreme_greedy_interval = 1000000,
        extreme_greedy_count = 1000,
        discount_rate = 0.99,
        frame_per_action = 4,
        model_sync_interval = 1000,
        batch_size = 1,
        epoch = 1,
        filename = "",
        action_list = None,
        action_map = None,
        network = None,
        ):
        if action_list is None or network is None:
            raise Exception('Parameter Error: <action_list> and <model> is required.')
        self.mario_course = mario_course    # マリオが学習するコース
        self.loop_count = loop_count    # 繰り返すエピソード数
        self.init_randam_num = init_randam_num  # ランダムに行動させる初期エピソード数
        self.greedy_rate = greedy_rate  # greedy率
        self.extreme_greedy_rate = extreme_greedy_rate  # 超greedy率
        self.extreme_greedy_interval = extreme_greedy_interval  # 超greedyを行うアクション間隔
        self.extreme_greedy_count = extreme_greedy_count    # 超greedy探索を行うアクション数
        self.discount_rate = discount_rate  # 次状態の割引率
        self.frame_per_action = frame_per_action    # 同一アクションを繰り返すフレーム数
        self.model_sync_interval = model_sync_interval  # Target Networkを、Q Networkに同期するアクション間隔
        self.batch_size = batch_size    # 一度に学習するサイズ
        self.epoch = epoch  # 一度の学習のエポック数
        self.filename = filename    # ロード・保存するファイル名
        self.action_list = action_list  # マリオがとりうるアクションをまとめたリスト
        self.action_map = action_map if action_map != None else [[i] for i in range(len(action_list))]  # 上記アクションリストの組み合わせを定義したリスト
        self.network = network
        self.action_list_len = len(self.action_map)
    
    # --- 学習しやすいように、入力画像を加工するための関数
    def image_processor(self, img):
        # new_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # new_img = cv2.resize(new_img, tuple(list(resized_screen_size)[0:-1]), interpolation=cv2.INTER_LINEAR)
        # new_img = np.reshape(new_img, resized_screen_size)
        # return new_img
        return img
    
    # --- 報酬の計算式
    def calc_reward(self, reward, is_finished, info, trunked_info):
        reward -= 1 # 止まっているだけのときの報酬をマイナスにしたい
        if is_finished and info['time'] != 0:
            if info['life'] == 0: # マリオが死んだとき
                reward -= 100
            else: # マリオがクリアしたとき
                reward += 300
        return reward, trunked_info


    def play(self):
        main_model = self.network.model
        target_model = self.network.clone_model()
        extreme_greedy_limit = self.extreme_greedy_interval + self.extreme_greedy_count
        loop_i = 1
        greedy_i = 1
        model_update_i = 1
        actions, screens = np.empty(0), np.empty(0)

        # --- マリオスタート、エピソード数繰り返す
        for episode in range(self.loop_count):
            env = gym.make(self.mario_course)
            env.reset()
            is_finished = False

            # --- 1回目の行動をする
            obs, reward, is_finished, info = env.step(self.action_list[0])
            trunked_info = {'old_distance': info['distance']}
            obs = np.reshape(obs, screen_size)
            screen = self.image_processor(obs)
            action_index = 0

            # --- ゲーム終了までアクション、学習を繰り返す
            while not is_finished:

                # --- 指定したフレームごとに行動を変える
                obs_array = np.empty(0)
                total_reward = 0
                action_set = self.action_map[action_index]
                for index, action in enumerate(action_set):
                    for i in range(0, self.frame_per_action):
                        # --- 行動を起こす
                        obs, reward, is_finished, info = env.step(self.action_list[action])
                        # --- 報酬を計算する
                        reward, trunked_info = self.calc_reward(reward, is_finished, info, trunked_info)
                        total_reward += reward
                        if is_finished:
                            break
                    # --- フレーム群の一番最後の観測状態を保存する
                    if index == len(action_set) -1 or is_finished:
                        last_obs = np.reshape(obs, screen_size)
                        last_obs = self.image_processor(last_obs)
                        if is_finished:
                            break
                obs = last_obs

                # --- 次状態の行動価値を、ニューラルネットワークから計算する
                next_values = main_model.predict(np.reshape(obs, tuple([1]) + resized_screen_size))[0]
                next_target_values = target_model.predict(np.reshape(obs, tuple([1]) + resized_screen_size))[0]

                greedy_rate = self.greedy_rate # --- ランダムに行動する確率
                if episode < self.init_randam_num:
                    greedy_rate = 1
                elif greedy_i > extreme_greedy_limit:
                    greedy_i = 0
                elif greedy_i > self.extreme_greedy_interval:
                    greedy_rate = self.extreme_greedy_rate

                # --- 次の行動パターンを決める
                if random.random() < greedy_rate:
                    next_action_index = random.randint(0, self.action_list_len-1)
                else :
                    next_action_index = np.argmax(next_values)

                # --- 現在の状態の行動価値を計算する
                if is_finished:
                    value = total_reward
                else:
                    value = np.max(next_target_values)
                    value = total_reward + self.discount_rate * value

                # --- 行動(出力)と画面(入力)をリストに保存
                action_array = np.full(self.action_list_len, np.inf)
                action_array[action_index] = value
                actions = np.append(actions, action_array)
                screens = np.append(screens, screen)

                # --- 学習サイズごとに学習を行う
                if loop_i == self.batch_size:
                    actions = np.reshape(actions, (-1, self.action_list_len))
                    screens = np.reshape(screens, tuple([-1]) + resized_screen_size)
                    history = main_model.fit(screens, actions, epochs=self.epoch)
                    actions, screens = np.empty(0), np.empty(0)
                    loop_i = 0


                if model_update_i >= self.model_sync_interval:
                    target_model = self.network.clone_model()
                    print("model synced.")
                    model_update_i = 0
    
                # --- 現在の状態の保存
                old_action_index = action_index
                action_index = next_action_index
                screen = obs
                
                loop_i += 1
                greedy_i += 1
                model_update_i += 1

                # --- 出力する(ゴールできたときはファイルにも)
                if is_finished and info['life'] != 0 and  info['time'] != 0:
                    if not os.path.exists('./goal_logs'):
                        os.makedirs('goal_logs')
                    with open(f'./goal_logs/{self.filename}', 'a') as f:
                        print("episode : " + str(episode), file=f)
                print('Episode : ', episode, 'Actions : ', [ self.action_list[i] for i in self.action_map[old_action_index] ], 'Value : ', value, 'Greedy : ', ('{:.3f}'.format(greedy_rate)))

            env.close()
            main_model.save('./saved_networks/' + self.filename)

        actions = np.reshape(actions, (-1, self.action_list_len))
        screens = np.reshape(screens, tuple([-1]) + resized_screen_size)
        main_model.fit(screens, actions, initial_epoch=0, epochs=self.epoch)

        main_model.save('./saved_networks/' + self.filename)

class DqnNetwork:
    def __init__(
        self,
        y_size = 10,
        ):
        self.y_size = y_size
    
    def create_custom_loss(self, y_true, y_pred):
        y_true = tf.where(y_true != np.inf, y_true, y_pred)
        return kb.mean((y_pred - y_true) ** 2)

    def create_network(self):
        initializer = TruncatedNormal(stddev=0.01)
        model = Sequential()
        model.add(Conv2D(64, kernel_size=(3, 3), strides=1, padding='same', activation='relu', input_shape=resized_screen_size, use_bias=True, kernel_initializer=initializer, bias_initializer=initializer))
        model.add(MaxPooling2D(2))
        model.add(Flatten())
        model.add(Dense(self.y_size, use_bias=True, kernel_initializer=initializer, bias_initializer=initializer))
        model.compile(optimizer='adam', loss=self.create_custom_loss, metrics=["accuracy"])
        self.model = model

    def clone_model(self):
        config = {
            'class_name': self.model.__class__.__name__,
            'config': self.model.get_config(),
        }
        clone = keras.models.model_from_config(config, custom_objects={"create_custom_loss": self.create_custom_loss})
        clone.set_weights(self.model.get_weights())
        return clone


if __name__ == '__main__' :
    # --- ニューラルネットワークの前回保存済みモデル、または今回保存モデル名を引数で取得する
    args = sys.argv
    if 2 <= len(args):
        filename = args[1]
    else:
        filename = None
    
    action_list = [
        [0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0], 
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 1, 1],
        [0, 1, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 1],
        [0, 1, 0, 0, 1, 1],
        [0, 0, 0, 1, 1, 0],
        [0, 0, 0, 1, 0, 1],
        [0, 0, 0, 1, 1, 1],
    ]
    action_map = [
        # [i, j] for i in range(len(action_list)) for j in range(len(action_list))
        [i] for i in range(len(action_list))
    ]

    network = DqnNetwork(
        y_size = len(action_map)
    )

    if filename != None and os.path.exists("./saved_networks/" + filename):
        # --- 前回保存済みモデルを読み込む
        network.model = keras.models.load_model("./saved_networks/" + filename, custom_objects={"create_custom_loss": network.create_custom_loss})
        print ("Successfully loaded")
    else:
        # --- 新たにニューラルネットワークを作成する
        if filename is None:
            filename = 'model'
        network.create_network()
        print ("Could not find old network weights")

    agent = Agent(
        mario_course = 'ppaquette/SuperMarioBros-1-1-Tiles-v0', # マリオが学習するコース
        loop_count = 30000, # 繰り返すエピソード数
        init_randam_num = 1,    # ランダムに行動させる初期エピソード数
        greedy_rate = 0.002,    # greedy率
        extreme_greedy_rate = 0.005,    # 超greedy率
        extreme_greedy_interval = 10000,    # 超greedyを行う間隔
        extreme_greedy_count = 500, # 超greedyを行うアクション数
        discount_rate = 0.9,    # 次状態の割引率
        frame_per_action = 6,   # 同一アクションを繰り返すフレーム数
        model_sync_interval = 640,  # Target Networkを、Q Networkに同期するアクション間隔
        batch_size = 64,    # 一度に学習するサイズ
        epoch = 10, # 一度の学習のエポック数
        filename = filename,    # ロード・保存するファイル名
        action_list = action_list,  # マリオがとりうるアクションをまとめたリスト
        action_map = action_map,    # 上記アクションリストの組み合わせを定義したリスト
        network = network
        )
    agent.play()
