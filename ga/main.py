# coding: cp932
import ga_main
import gym
import ppaquette_gym_super_mario

def main():
    n_gene   = 6*3000 # ��`�q��
    n_ind    = 60   # �̐�
    CXPB     = 0.5   # ������
    MUTPB    = 0.5   # �̂̓ˑR�ψٗ�
    MUTINDPB = 0.01  # ��`�q�̓ˑR�ψٗ�
    NGEN     = 30    # ���㐔
    
    ga_main.proceed(n_gene, n_ind, CXPB, MUTPB, MUTINDPB, NGEN, evalOneMax, continued = False)


def evalOneMax(ind):
    """�K���x�v�Z�֐�"""
    env = gym.make('ppaquette/meta-SuperMarioBros-v0')
    env.reset()
    i = 0
    while True:
        if i >= len(ind):
            break
        action = ind[i:i+6]
        observation, reward, done, info = env.step(action)
        if done == True:
            break
        i += 6
    env.close()
    return info['total_reward']

if __name__ == "__main__":
    main()
