#  coding: cp932
import ga_methods as ga
import random
import numpy as np
from operator import attrgetter
import pickle
import os

def proceed(n_gene, n_ind, CXPB, MUTPB, MUTINDPB, NGEN, eval_func, continued = False):
    """
    与えられたパラメータを使ってGAを実行する

    Parameters
    ----------
    n_gene : int
        1個体あたりの遺伝子数。
    n_ind : int
        個体数。
    CXBP : double
        交叉率。
    MUTPB : double
        個体の突然変異率。
    MUTINDPB : double
        遺伝子の突然変異率。
    MGEN : int
        世代数。
    eval_func : function
        適応度計算関数。
    continued : boolean
        前回の続きを行うかどうか。
    """

    random.seed(64)
    init_gen = 0
    # --- Step1 : 第一世代の個体群を作成(前回の続きを行う場合、パラメータの引継ぎ)
    filename = 'session.pickle'
    if continued and os.path.isfile(filename):
        with open(filename, 'rb') as f:
            objs = pickle.load(f)
        pop = objs['pop']
        n_gene = objs['n_gene']
        n_ind = objs['n_ind']
        init_gen = objs['init_gen']
    else:
        pop = create_pop(n_ind, n_gene)   

    ga.set_fitness(eval_func, pop)
    best_ind = max(pop, key=attrgetter("fitness"))
    print("Generation loop start.")
    print("Generation:  " + str(init_gen) + ". Best fitness: " + str(best_ind.fitness))
    
    # --- 世代を繰り返す
    for g in range(init_gen, init_gen + NGEN):
        # --- Step2 : トーナメント選択
        offspring = ga.selTournament(pop, n_ind, tournsize=3)
        
        # --- Step3 : 2点交叉
        crossover = []
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < CXPB:
                child1, child2 = ga.cxTwoPointCopy(child1, child2)
                child1.fitness = None
                child2.fitness = None
            crossover.append(child1)
            crossover.append(child2)

        offspring = crossover[:]
        
        # --- Step4 : 突然変異
        mutant = []
        for mut in offspring:
            if random.random() < MUTPB:
                mut = ga.mutFlipBit(mut, indpb=MUTINDPB)
                mut.fitness = None
            mutant.append(mut)

        offspring = mutant[:]
        
        # --- 次世代の個体群を作成
        pop = offspring[:]
        ga.set_fitness(eval_func, pop)
        
        # --- 最優良個体の探索（表示）・次世代への引継ぎ
        tmp_best_ind = max(pop, key=attrgetter("fitness"))
        if best_ind.fitness > tmp_best_ind.fitness:
            pop[0] = best_ind
        else:
            best_ind = tmp_best_ind
        print("Generation: " + str(g+1) + ". Best fitness: " + str(best_ind.fitness))
    
    print("Generation loop ended. The best individual: ")
    np.set_printoptions(threshold=np.inf)
    print(best_ind)

    # --- 今回終了したところまでの結果を保存
    objs = {
        'pop' : pop,
        'n_gene' : n_gene,
        'n_ind' : n_ind,
        'init_gen' : g+1
    }
    with open('session.pickle', 'wb') as f:
        pickle.dump(objs, f)
        
class Individual(np.ndarray):
    """個体"""
    fitness = None
    def __new__(cls, a):
        return np.asarray(a).view(cls)

def create_ind(n_gene):
    """個体作成関数"""
    return Individual([random.randint(0, 1) for i in range(n_gene)])

def create_pop(n_ind, n_gene):
    """個体群作成関数"""
    pop = []
    for i in range(n_ind):
        ind = create_ind(n_gene)
        pop.append(ind)
    return pop