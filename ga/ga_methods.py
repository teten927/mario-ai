# coding: cp932
import random
from multiprocessing import Pool
import multiprocessing
from operator import attrgetter

random.seed(64)

def set_fitness(eval_func, pop):
    """適応度を計算し、セットする関数"""
    with Pool(multiprocessing.cpu_count()) as pool:
        for i, fit in zip(range(len(pop)), pool.map(eval_func, pop)):
            pop[i].fitness = fit

def selTournament(pop, n_ind, tournsize):
    """トーナメント選択を行う関数"""
    chosen = []    
    for i in range(n_ind):
        aspirants = [random.choice(pop) for j in range(tournsize)]
        chosen.append(max(aspirants, key=attrgetter("fitness")))
    return chosen

def cxTwoPointCopy(ind1, ind2):
    """交叉を行う関数"""
    size = len(ind1)
    tmp1 = ind1.copy()
    tmp2 = ind2.copy()
    cxpoint1 = random.randint(1, size)
    cxpoint2 = random.randint(1, size-1)
    if cxpoint2 >= cxpoint1:
        cxpoint2 += 1
    else: # cxpointで交叉する
        cxpoint1, cxpoint2 = cxpoint2, cxpoint1
    tmp1[cxpoint1:cxpoint2], tmp2[cxpoint1:cxpoint2] = tmp2[cxpoint1:cxpoint2].copy(), tmp1[cxpoint1:cxpoint2].copy()
    return tmp1, tmp2

def mutFlipBit(ind, indpb):
    """突然変異を行う関数"""
    tmp = ind.copy()
    for i in range(len(ind)):
        if random.random() < indpb:
            tmp[i] = type(ind[i])(not ind[i])
    return tmp
